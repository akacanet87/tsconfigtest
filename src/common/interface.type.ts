export interface RootState {
  version: string;
}

export interface FacebookDataType {
  id: string;
  authorization_token: string;
  state: string;
  refresh_token: string;
}

