const AUTH_TOKEN_KEY = 'auth_token';
const REFRESH_TOKEN_KEY = 'refresh_token';
const FACEBOOK_SIGN_ID_KEY = 'facebook_sign_id';
const FACEBOOK_STATE_KEY = 'facebook_state';

const authJWTList = {
  getAuthToken() {
    return window.localStorage.getItem(AUTH_TOKEN_KEY);
  },
  saveAuthToken(token: string) {
    window.localStorage.setItem(AUTH_TOKEN_KEY, token);
  },
  destroyAuthToken() {
    window.localStorage.removeItem(AUTH_TOKEN_KEY);
  },
  getRefreshToken() {
    return window.localStorage.getItem(REFRESH_TOKEN_KEY);
  },
  saveRefreshToken(token: string) {
    window.localStorage.setItem(REFRESH_TOKEN_KEY, token);
  },
  destroyRefreshToken() {
    window.localStorage.removeItem(REFRESH_TOKEN_KEY);
  },
  getFacebookSignId() {
    return window.localStorage.getItem(FACEBOOK_SIGN_ID_KEY);
  },
  saveFacebookSignId(id: string) {
    window.localStorage.setItem(FACEBOOK_SIGN_ID_KEY, id);
  },
  destroyFacebookSignId() {
    window.localStorage.removeItem(FACEBOOK_SIGN_ID_KEY);
  },
  getFacebookState() {
    return window.localStorage.getItem(FACEBOOK_STATE_KEY);
  },
  saveFacebookState(state: string) {
    window.localStorage.setItem(FACEBOOK_STATE_KEY, state);
  },
  destroyFacebookState() {
    window.localStorage.removeItem(FACEBOOK_STATE_KEY);
  },
};

export default authJWTList;
