//TODO: Between typescript file, import has no problem
import { FacebookDataType } from '@/common/interface.type';

const regexList = {
  isEmail(email: string) {
    return /^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/.test(email);
  },
  isOnlyNumber(text: string) {
    return /^[0-9]+$/.test(text);
  },
  isToken(token: FacebookDataType) {
    return token;
  },
};

export default regexList;
