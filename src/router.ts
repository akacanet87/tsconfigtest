import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/*const authRequired = () => (from: any, to: any, next: any) => {
  const isAuthorized = false;
  if (isAuthorized) {
    return next('/');
  }
  next('/my_page');
};*/

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "test" */ './views/Test.vue'),
    },
    {
      path: '/sign_in',
      name: 'sign_in',
      component: () => import(/* webpackChunkName: "signIn" */ './views/SignIn.vue'),
    },
    {
      path: '/result',
      name: 'result',
      component: () => import(/* webpackChunkName: "result" */ './views/Result.vue'),
    },
  ],
});
